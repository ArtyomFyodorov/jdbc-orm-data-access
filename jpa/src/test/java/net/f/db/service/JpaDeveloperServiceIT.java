package net.f.db.service;

import net.f.db.entity.Developer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Artyom Fyodorov
 */
class JpaDeveloperServiceIT {

    private JpaDeveloperService dsut;

    @BeforeEach
    void initDeveloperService() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("h2-integration-test");
        this.dsut = new JpaDeveloperService(emf);
    }

    @Test
    void testFindAll() {
        this.dsut.saveOrUpdate(Developer.getRandom());
        List<Developer> results = this.dsut.findAll();

        assertTrue(results.size() > 0);
    }

    @Test
    void testFindByName() {
        // prepare
        String developerName = this.dsut.saveOrUpdate(Developer.getRandom()).getName();
        // test
        List<Developer> results = this.dsut.findByName(developerName);
        assertTrue(results.size() > 0);
    }

    @Test
    void testFindNameById() {
        // prepare
        Developer developer = Developer.getRandom();
        String expectedName = developer.getName();
        Long developerId = this.dsut.saveOrUpdate(developer).getId();
        // test
        String actualName = this.dsut.findNameById(developerId);
        assertThat(actualName, is(expectedName));
    }

    @Test
    void testFindById() {
        // prepare
        Developer expected = this.dsut.saveOrUpdate(Developer.getRandom());
        // test
        Developer actual = this.dsut.findById(expected.getId());
        assertEquals(expected, actual);
    }

    @Test
    void testSaveOrUpdate() {
        assertNotNull(this.dsut.saveOrUpdate(Developer.getRandom()).getId());
    }

    @Test
    void testRemove() {
        // prepare
        Long developerId = this.dsut.saveOrUpdate(Developer.getRandom()).getId();
        // test
        this.dsut.remove(developerId);
        assertThrows(NoResultException.class, () -> this.dsut.findById(developerId));
    }
}