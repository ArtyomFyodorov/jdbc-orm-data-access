package net.f.db.service;

import net.f.db.entity.Developer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class JpaDeveloperService implements DeveloperService {

    private final EntityManager em;
    private final EntityTransaction tx;

    public JpaDeveloperService(EntityManagerFactory emf) {
        if (emf == null) {
            throw new IllegalArgumentException("EntityManagerFactory must not be null");
        }

        this.em = emf.createEntityManager();
        this.tx = this.em.getTransaction();
    }

    @Override
    public List<Developer> findAll() {
        this.tx.begin();
        try {
            CriteriaBuilder cb = this.em.getCriteriaBuilder();
            CriteriaQuery<Developer> cq = cb.createQuery(Developer.class);
            Root<Developer> root = cq.from(Developer.class);
            cq.select(root);

            return this.em.createQuery(cq).getResultList();
        } finally {
            this.tx.commit();
        }
    }

    @Override
    public List<Developer> findByName(String name) {
        this.tx.begin();
        try {
            CriteriaBuilder cb = this.em.getCriteriaBuilder();
            CriteriaQuery<Developer> cq = cb.createQuery(Developer.class);
            Root<Developer> root = cq.from(Developer.class);
            cq.select(root).where(cb.equal(root.get("name").as(String.class), name));

            return this.em.createQuery(cq).getResultList();
        } finally {
            this.tx.commit();
        }
    }

    @Override
    public String findNameById(Long id) {
        this.tx.begin();
        try {
            CriteriaBuilder cb = this.em.getCriteriaBuilder();
            CriteriaQuery<String> cq = cb.createQuery(String.class);
            Root<Developer> root = cq.from(Developer.class);
            cq.select(root.get("name").as(String.class)).where(cb.equal(root.get("id").as(Long.class), id));

            return this.em.createQuery(cq).getSingleResult();
        } finally {
            this.tx.commit();
        }
    }

    @Override
    public Developer findById(Long id) {
        this.tx.begin();
        try {
            CriteriaBuilder cb = this.em.getCriteriaBuilder();
            CriteriaQuery<Developer> cq = cb.createQuery(Developer.class);
            Root<Developer> root = cq.from(Developer.class);
            cq.select(root).where(cb.equal(root.get("id").as(Long.class), id));

            return this.em.createQuery(cq).getSingleResult();
        } finally {
            this.tx.commit();
        }
    }

    @Override
    public Developer saveOrUpdate(Developer developer) {
        this.tx.begin();
        try {
            if (developer.getId() == null) {
                em.persist(developer);
                return developer;
            } else {
                return em.merge(developer);
            }
        } finally {
            this.tx.commit();
        }
    }

    @Override
    public void remove(Long id) {
        this.tx.begin();
        Developer removableEntity = this.em.find(Developer.class, id);
        this.em.remove(removableEntity);
        this.tx.commit();
    }
}
