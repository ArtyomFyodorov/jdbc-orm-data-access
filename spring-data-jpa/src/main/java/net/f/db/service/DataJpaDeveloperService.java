package net.f.db.service;

import net.f.db.entity.Developer;
import net.f.db.repository.DeveloperRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;
import java.util.stream.StreamSupport;

import static java.util.stream.Collectors.toList;

/**
 * @author Artyom Fyodorov
 */
@Service
@Transactional(readOnly = true)
public class DataJpaDeveloperService implements DeveloperService {

    private final DeveloperRepository developerRepository;

    public DataJpaDeveloperService(DeveloperRepository developerRepository) {
        Assert.notNull(developerRepository, "DeveloperRepository must not be null");
        this.developerRepository = developerRepository;
    }

    @Override
    public List<Developer> findAll() {
        return StreamSupport.stream(
                developerRepository.findAll().spliterator(),
                false)
                .collect(toList());
    }

    @Override
    public List<Developer> findByName(String name) {
        return developerRepository.findByName(name);
    }

    @Override
    public String findNameById(Long id) {
        return developerRepository.findNameById(id)
                .orElseThrow(() -> new ObjectNotFoundException(
                        id, String.class.getSimpleName()))
                .getName();
    }

    @Override
    public Developer findById(Long id) {
        return developerRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(
                        id, Developer.class.getSimpleName()));
    }

    @Transactional
    @Override
    public Developer saveOrUpdate(Developer developer) {
        return developerRepository.save(developer);
    }

    @Transactional
    @Override
    public void remove(Long id) {
        developerRepository.findById(id)
                .ifPresent(developerRepository::delete);
    }
}
