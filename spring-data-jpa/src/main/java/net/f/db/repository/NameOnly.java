package net.f.db.repository;

/**
 * @author Artyom Fyodorov
 */
public interface NameOnly {
    String getName();
}
