package net.f.db.repository;

import net.f.db.entity.Developer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * @author Artyom Fyodorov
 */
@Transactional(readOnly = true)
public interface DeveloperRepository extends CrudRepository<Developer, Long> {

    List<Developer> findByName(String name);

    Optional<NameOnly> findNameById(@Param("id") Long id);
}
