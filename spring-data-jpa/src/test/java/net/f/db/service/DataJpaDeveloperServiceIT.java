package net.f.db.service;

import net.f.db.configuration.DataJpaConfiguration;
import net.f.db.entity.Developer;
import org.hibernate.ObjectNotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Artyom Fyodorov
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = DataJpaConfiguration.class)
class DataJpaDeveloperServiceIT {
    @Autowired
    private DeveloperService dsut;
    private Developer wellKnownDeveloper;

    @BeforeEach
    void initDeveloperService() {
        this.wellKnownDeveloper = this.dsut.saveOrUpdate(Developer.getRandom());
    }

    @Test
    void testFindAll() {
        List<Developer> results = this.dsut.findAll();

        assertTrue(results.size() > 0);
    }

    @Test
    void testFindByName() {
        String developerName = this.wellKnownDeveloper.getName();
        List<Developer> results = this.dsut.findByName(developerName);
        assertTrue(results.size() > 0);
    }

    @Test
    void testFindNameById() {
        String expectedName = this.wellKnownDeveloper.getName();
        String actualName = this.dsut.findNameById(this.wellKnownDeveloper.getId());
        assertThat(actualName, is(expectedName));
    }

    @Test
    void testFindById() {
        Developer actual = this.dsut.findById(this.wellKnownDeveloper.getId());
        assertEquals(this.wellKnownDeveloper, actual);
    }

    @Test
    void testSaveOrUpdate() {
        assertNotNull(this.dsut.saveOrUpdate(Developer.getRandom()).getId());
    }

    @Test
    void testRemove() {
        Long developerId = this.wellKnownDeveloper.getId();
        this.dsut.remove(developerId);
        assertThrows(ObjectNotFoundException.class, () -> this.dsut.findById(developerId));
    }
}