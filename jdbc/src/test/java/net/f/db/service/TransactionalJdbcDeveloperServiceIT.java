package net.f.db.service;

import net.f.db.dao.DeveloperDao;
import net.f.db.dao.JdbcConnection;
import net.f.db.dao.JdbcDeveloperDao;
import net.f.db.entity.Developer;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author Artyom Fyodorov
 */
class TransactionalJdbcDeveloperServiceIT {

    private DeveloperService dsut;
    private Developer wellKnownDeveloper;

    @BeforeEach
    void init() throws Exception {
        DataSource dataSource = getDataSource();
        JdbcConnection jdbcConnection = new JdbcConnection(dataSource);
        DeveloperDao developerDao = new JdbcDeveloperDao(jdbcConnection);
        TransactionManager txManager = new CustomTransactionManager(jdbcConnection);
        this.dsut = new TransactionalJdbcDeveloperService(txManager, developerDao);

        dsut.createTable();
        this.wellKnownDeveloper = this.dsut.saveOrUpdate(Developer.getRandom());
    }

    @AfterEach
    void tearDown() {
        dsut.remove(wellKnownDeveloper.getId());
    }

    private DataSource getDataSource() throws IOException {
        final Properties properties = new Properties();
        try (InputStream in = Files.newInputStream(Paths.get("src/main/resources/db/database.properties"))) {
            properties.load(in);
        }

        String url = properties.getProperty("h2.url");
        String username = properties.getProperty("h2.username");
        String password = properties.getProperty("h2.password");

        return JdbcConnectionPool.create(url, username, password);
    }

    @Test
    void testFindAll() {
        List<Developer> results = this.dsut.findAll();
        final int expNumOfDevs = 1;
        assertEquals(expNumOfDevs, results.size());
        assertEquals(wellKnownDeveloper.getLanguages().size(), results.get(0).getLanguages().size());
    }

    @Test
    void testFindByName() {
        String developerName = this.wellKnownDeveloper.getName();
        List<Developer> results = this.dsut.findByName(developerName);
        final int expNumOfDevs = 1;
        assertEquals(expNumOfDevs, results.size());
        assertEquals(wellKnownDeveloper.getLanguages().size(), results.get(0).getLanguages().size());
    }

    @Test
    void testFindNameById() {
        String expectedName = this.wellKnownDeveloper.getName();
        String actualName = this.dsut.findNameById(this.wellKnownDeveloper.getId());
        assertThat(actualName, is(expectedName));
    }

    @Test
    void testFindById() {
        Developer actual = this.dsut.findById(this.wellKnownDeveloper.getId());
        assertEquals(this.wellKnownDeveloper, actual);
    }

    @Test
    void testSaveOrUpdate() {
        assertNotNull(this.dsut.saveOrUpdate(Developer.getRandom()).getId());
    }

    @Test
    void testRemove() {
        Long developerId = this.wellKnownDeveloper.getId();
        this.dsut.remove(developerId);
        assertNull(this.dsut.findById(developerId));
    }
}