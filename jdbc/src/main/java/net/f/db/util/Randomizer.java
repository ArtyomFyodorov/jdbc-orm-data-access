package net.f.db.util;

import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Artyom Fyodorov
 */
public class Randomizer {
    private static final String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyz";

    /**
     * @param min -  the least value returned
     * @param max - the upper bound (exclusive)
     * @return a pseudorandom String value between the min (inclusive) and the max (exclusive)
     */
    public static String randomAlphaNumeric(int min, int max) {
        StringBuilder builder = new StringBuilder();
        int count = ThreadLocalRandom.current().nextInt(min, max);
        while (count-- != 0) {
            int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        builder.replace(0, 1, builder.substring(0, 1).toUpperCase());

        return builder.toString();
    }
}
