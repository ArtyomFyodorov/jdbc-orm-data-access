package net.f.db.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import static net.f.db.util.Randomizer.randomAlphaNumeric;

/**
 * @author Artyom Fyodorov
 */
public class Developer implements Serializable {

    private static final long serialVersionUID = 6399187408373536438L;

    private Long id;
    private String name;
    private Set<String> languages = new HashSet<>();

    public Developer() {
    }

    public Developer(String name) {
        this.name = name;
    }

    public static Developer getRandom() {
        Developer duke = new Developer(randomAlphaNumeric(5, 10));
        Set<String> languages = new HashSet<>();
        languages.add(randomAlphaNumeric(4, 8));
        languages.add(randomAlphaNumeric(5, 8));
        languages.add(randomAlphaNumeric(6, 8));
        duke.setLanguages(languages);

       /* Developer duke = new Developer("duke");
        Set<String> languages = new HashSet<>();
        languages.add("java");
        languages.add("scala");
        languages.add("kotlin");
        duke.setLanguages(languages);*/

        return duke;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addLanguage(String language) {
        getLanguages().add(language);
    }

    public Set<String> getLanguages() {
        if (languages == null) languages = new HashSet<>();
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Developer)) return false;
        Developer developer = (Developer) o;
        return Objects.equals(id, developer.id) &&
                Objects.equals(name, developer.name) &&
                Objects.equals(languages, developer.languages);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, languages);
    }

    @Override
    public String toString() {
        return "Developer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", languages=" + languages +
                '}';
    }
}
