package net.f.db.service;

import net.f.db.dao.JdbcConnection;

import java.util.function.Supplier;

/**
 * @author Artyom Fyodorov
 */
public class CustomTransactionManager implements TransactionManager {
    private final JdbcConnection connection;

    public CustomTransactionManager(JdbcConnection connection) {
        if (connection == null) {
            throw new IllegalArgumentException("JdbcConnection must not be null");
        }
        this.connection = connection;
    }

    @Override
    public <T> T execute(Supplier<T> unitOfWork) {
        try {
            connection.begin();
            T result = unitOfWork.get();
            connection.commit();
            return result;
        } catch (Throwable e) {
            connection.rollback();
            throw new RuntimeException(e);
        } finally {
            connection.closeConnection();
        }
    }
}
