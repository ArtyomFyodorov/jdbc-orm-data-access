package net.f.db.service;

import net.f.db.entity.Developer;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public interface DeveloperService {
    List<Developer> findAll();

    List<Developer> findByName(String name);

    String findNameById(Long id);

    Developer findById(Long id);

    Developer saveOrUpdate(Developer developer);

    void remove(Long id);

    default void createTable() {
        throw new UnsupportedOperationException("createTable");
    }
}
