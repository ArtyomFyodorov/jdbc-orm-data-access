package net.f.db.service;

import net.f.db.dao.DeveloperDao;
import net.f.db.entity.Developer;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public class TransactionalJdbcDeveloperService implements DeveloperService {

    private final TransactionManager txManager;
    private final DeveloperDao developerDao;

    public TransactionalJdbcDeveloperService(TransactionManager txManager, DeveloperDao developerDao) {
        if (txManager == null) {
            throw new IllegalArgumentException("TransactionManager must not be null");
        }
        if (developerDao == null) {
            throw new IllegalArgumentException("DeveloperDao must not be null");
        }
        this.txManager = txManager;
        this.developerDao = developerDao;
    }

    @Override
    public List<Developer> findAll() {
        return txManager.execute(developerDao::findAll);
    }

    @Override
    public List<Developer> findByName(String name) {
        return txManager.execute(() -> developerDao.findByName(name));
    }

    @Override
    public String findNameById(Long id) {
        return txManager.execute(() -> developerDao.findNameById(id));
    }

    @Override
    public Developer findById(Long id) {
        return txManager.execute(() -> developerDao.findById(id));
    }

    @Override
    public Developer saveOrUpdate(Developer developer) {
        return developer.getId() == null
                ? txManager.execute(() -> developerDao.insert(developer))
                : txManager.execute(() -> developerDao.update(developer));
    }

    @Override
    public void remove(Long id) {
        txManager.execute(() -> developerDao.delete(id));
    }

    @Override
    public void createTable() {
        txManager.execute(developerDao::createTable);
    }
}
