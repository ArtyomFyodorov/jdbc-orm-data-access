package net.f.db.service;

import java.util.function.Supplier;

/**
 * @author Artyom Fyodorov
 */
public interface TransactionManager {
    <T> T execute(Supplier<T> unitOfWork);
}
