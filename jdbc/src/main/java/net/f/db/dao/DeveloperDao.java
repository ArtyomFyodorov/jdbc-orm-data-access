package net.f.db.dao;

import net.f.db.entity.Developer;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
public interface DeveloperDao {

    List<Developer> findAll();

    List<Developer> findByName(String name);

    String findNameById(Long id);

    Developer findById(Long id);

    Developer insert(Developer developer);

    Developer update(Developer developer);

    boolean delete(Long id);

    default boolean createTable() {
        throw new UnsupportedOperationException("createTable");
    }
}
