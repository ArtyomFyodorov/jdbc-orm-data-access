package net.f.db.dao;

import net.f.db.entity.Developer;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
public class JdbcDeveloperDao implements DeveloperDao {
    private static final Logger logger = Logger.getLogger(JdbcDeveloperDao.class.getName());
    private final JdbcConnection jdbcConnection;

    public JdbcDeveloperDao(JdbcConnection jdbcConnection) {
        if (jdbcConnection == null) {
            throw new IllegalArgumentException("JdbcConnection must not be null");
        }
        this.jdbcConnection = jdbcConnection;
    }

    public boolean createTable() {
        final String createDevelopersTableSql =
                "CREATE TABLE IF NOT EXISTS developers (" +
                        "id BIGINT NOT NULL AUTO_INCREMENT" +
                        ", name VARCHAR(255)" +
                        ", PRIMARY KEY (id))";
        final String createLanguagesTableSql =
                "CREATE TABLE IF NOT EXISTS languages (" +
                        "developer_id BIGINT NOT NULL," +
                        "value VARCHAR(255))";

        Connection connection = this.jdbcConnection.getConnection();
        try (Statement stmt = connection.createStatement()) {
            stmt.executeUpdate(createDevelopersTableSql);
            logger.info("table 'developers' has been successfully created.");
            stmt.executeUpdate(createLanguagesTableSql);
            logger.info("table 'languages' has been successfully created.");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return true;
    }

    @Override
    public List<Developer> findAll() {
        Connection connection = this.jdbcConnection.getConnection();
        try (Statement stmt = connection.createStatement()) {
            ResultSet rs = stmt.executeQuery("SELECT d.id, d.name, l.value FROM developers AS d " +
                    "LEFT OUTER JOIN languages AS l ON d.id = l.developer_id");
            List<Developer> result = new ArrayList<>();
            while (rs.next()) {
                result.add(getDeveloperWithLanguages(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Developer> findByName(String name) {
        final String sql = "SELECT d.id, d.name, l.value FROM developers AS d " +
                "LEFT OUTER JOIN languages AS l ON d.id = l.developer_id WHERE name = ?";

        Connection connection = jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, name);
            ResultSet rs = ps.executeQuery();
            List<Developer> result = new ArrayList<>();
            while (rs.next()) {
                result.add(getDeveloperWithLanguages(rs));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String findNameById(Long id) {
        final String sql = "SELECT name FROM developers WHERE id = ?";

        Connection connection = jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getString("name");
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Developer findById(Long id) {
        final String sql = "SELECT d.id, d.name, l.value FROM developers AS d " +
                "LEFT OUTER JOIN languages AS l ON d.id = l.developer_id WHERE d.id = ?";

        Connection connection = jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return getDeveloperWithLanguages(rs);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Developer insert(Developer developer) {
        logger.info("developer arrived: " + developer);

        final String sql = "INSERT INTO developers(name) VALUES(?)";

        Connection connection = this.jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql,
                Statement.RETURN_GENERATED_KEYS)) {
            ps.setString(1, developer.getName());
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs.next()) {
                long developerId = rs.getLong(1);
                developer.setId(developerId);
                insertLanguages(connection, developerId, developer.getLanguages());
            }
            logger.info("developer stored with id: " + developer.getId());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return developer;
    }

    @Override
    public Developer update(Developer developer) {
        logger.info("developer arrived: " + developer);

        final String sql = "UPDATE developers SET name = ? WHERE id = ?";

        Connection connection = this.jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, developer.getName());
            ps.setLong(2, developer.getId());
            ps.executeUpdate();
            long developerId = developer.getId();
            updateLanguages(connection, developerId, developer.getLanguages());
            logger.info("developer updated: " + developer);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return developer;
    }

    @Override
    public boolean delete(Long id) {
        final String sql = "DELETE FROM developers WHERE id = ?";

        Connection connection = this.jdbcConnection.getConnection();
        try (PreparedStatement ps = connection.prepareStatement(
                sql)) {
            ps.setLong(1, id);
            final int result = ps.executeUpdate();
            deleteLanguages(connection, id);
            if (result > 0) {
                logger.info("Deleting developer with id: " + id);
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Developer getDeveloperWithLanguages(ResultSet rs) throws SQLException {
        Developer developer = getDeveloper(rs);
        Set<String> languages = getLanguagesOfTheCurrentDeveloper(rs);
        developer.setLanguages(languages);

        return developer;
    }

    private Developer getDeveloper(ResultSet rs) throws SQLException {
        Developer developer = new Developer();
        developer.setId(rs.getLong("id"));
        developer.setName(rs.getString("name"));

        return developer;
    }

    private Set<String> getLanguagesOfTheCurrentDeveloper(ResultSet rs) throws SQLException {
        Set<String> languages = new HashSet<>();
        do {
            languages.add(rs.getString("value"));
        } while (rs.next());

        return languages;
    }

    private void insertLanguages(Connection connection, long developerId, Set<String> languages) throws SQLException {
        final String sql = "INSERT INTO languages VALUES(?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            for (String language : languages) {
                ps.setLong(1, developerId);
                ps.setString(2, language);
                ps.addBatch();
            }
            ps.executeBatch();
        }
    }

    private void updateLanguages(Connection connection, Long developerId, Set<String> languages) throws SQLException {
        deleteLanguages(connection, developerId);
        insertLanguages(connection, developerId, languages);
    }

    private void deleteLanguages(Connection connection, long developerId) {
        final String sql = "DELETE FROM languages WHERE developer_id = ?";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setLong(1, developerId);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
