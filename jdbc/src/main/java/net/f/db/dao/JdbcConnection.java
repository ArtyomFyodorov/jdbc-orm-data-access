package net.f.db.dao;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

import static java.lang.Boolean.FALSE;

/**
 * @author Artyom Fyodorov
 */
public class JdbcConnection {
    private static final Logger logger = Logger.getLogger(JdbcConnection.class.getName());

    private final DataSource dataSource;
    private final ThreadLocal<Connection> tlConnection = new ThreadLocal<>();

    public JdbcConnection(DataSource dataSource) {
        if (dataSource == null) {
            throw new IllegalArgumentException("DataSource must not be null");
        }
        this.dataSource = dataSource;
    }

    public Connection getConnection() {
        Connection connection = this.tlConnection.get();
        if (connection == null) {
            try {
                connection = dataSource.getConnection();
            } catch (SQLException e) {
                logger.severe("Problem getting tlConnection to the database");
                throw new RuntimeException(e);
            }
            this.tlConnection.set(connection);
        }
        return connection;
    }

    public void closeConnection() {
        Connection connection = this.tlConnection.get();
        try {
            if (connection != null) connection.close();
            logger.info("Connection closed");
        } catch (SQLException e) {
            logger.severe("Problem closing tlConnection to the database");
            throw new RuntimeException(e);
        }
        this.tlConnection.remove();
    }

    public Connection begin() {
        Connection connection = getConnection();
        try {
            connection.setAutoCommit(FALSE);
            logger.info("Transaction beginning");
        } catch (SQLException e) {
            closeConnection();
            throw new RuntimeException(e);
        }

        return connection;
    }

    public void rollback() {
        try {
            getConnection().rollback();
            logger.severe("Transaction rollback");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void commit() {
        try {
            getConnection().commit();
            logger.info("Transaction committed");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
