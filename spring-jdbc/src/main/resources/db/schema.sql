CREATE TABLE developers (
    id INT NOT NULL AUTO_INCREMENT
  , name VARCHAR(255) NOT NULL
  , PRIMARY KEY (id)
);

CREATE TABLE languages (
    developer_id BIGINT NOT NULL
  , value VARCHAR(255)
  , UNIQUE uq_languages_1 (developer_id, value)
);