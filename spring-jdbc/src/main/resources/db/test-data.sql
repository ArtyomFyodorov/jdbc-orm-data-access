insert into developers (name) values ('Duke');
insert into developers (name) values ('Duchess');
insert into developers (name) values ('Mock');

insert into languages (developer_id, value) values (1, 'Java');
insert into languages (developer_id, value) values (1, 'Scala');
insert into languages (developer_id, value) values (2, 'Kotlin');
