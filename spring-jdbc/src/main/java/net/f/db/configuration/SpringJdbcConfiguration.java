package net.f.db.configuration;

import net.f.db.dao.DeveloperDao;
import net.f.db.dao.SpringJdbcDeveloperDao;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
@Configuration
@ComponentScan("net.f.db")
@PropertySource("classpath:db/database.properties")
@EnableTransactionManagement
public class SpringJdbcConfiguration {
    private static final Logger logger = Logger.getLogger(SpringJdbcConfiguration.class.getName());

    @Value("${h2.url}")
    private String url;
    @Value("${h2.username}")
    private String username;
    @Value("${h2.password}")
    private String password;

    @Bean
    public EmbeddedDatabase dataSource() {
        try {
            EmbeddedDatabaseBuilder dbBuilder = new EmbeddedDatabaseBuilder();
            return dbBuilder.setType(EmbeddedDatabaseType.H2)
                    .addScripts("classpath:db/schema.sql", "classpath:db/test-data.sql").build();
        } catch (Exception e) {
            logger.severe("Embedded DataSource bean cannot be created");
            e.printStackTrace();
            return null;
        }
    }

    @Bean(destroyMethod = "destroy")
    public CleanUp cleanUp() {
        return new CleanUp(new JdbcTemplate(dataSource()));
    }

    @Bean
    public PlatformTransactionManager transactionManager(EmbeddedDatabase dataSource) {
        return new DataSourceTransactionManager(dataSource);
    }

    @Bean
    public NamedParameterJdbcTemplate jdbcTemplate(EmbeddedDatabase dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    public DeveloperDao developerDao(DataSource dataSource, NamedParameterJdbcTemplate jdbcTemplate) {
        return new SpringJdbcDeveloperDao(dataSource, jdbcTemplate);
    }

}
