package net.f.db.configuration;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.logging.Logger;

/**
 * @author Artyom Fyodorov
 */
public class CleanUp {
    private static final Logger logger = Logger.getLogger(CleanUp.class.getName());

    private JdbcTemplate jdbcTemplate;

    public CleanUp(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private void destroy() {
        logger.info(" ... Deleting database files.");
        jdbcTemplate.execute("DROP ALL OBJECTS DELETE FILES;");
    }
}
