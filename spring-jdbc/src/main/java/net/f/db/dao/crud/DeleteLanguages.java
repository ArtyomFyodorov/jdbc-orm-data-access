package net.f.db.dao.crud;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

/**
 * @author Artyom Fyodorov
 */
public class DeleteLanguages extends SqlUpdate {
    private static final String SQL_DELETE_LANGUAGES =
            "DELETE FROM languages WHERE developer_id = :developerId";

    public DeleteLanguages(DataSource dataSource) {
        super(dataSource, SQL_DELETE_LANGUAGES);
        super.declareParameter(new SqlParameter("developerId", Types.BIGINT));
    }
}
