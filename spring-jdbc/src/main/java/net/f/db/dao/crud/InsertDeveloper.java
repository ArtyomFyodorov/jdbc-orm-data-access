package net.f.db.dao.crud;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

import static java.lang.Boolean.TRUE;

/**
 * @author Artyom Fyodorov
 */
public class InsertDeveloper extends SqlUpdate {
    private static final String SQL_INSERT_DEVELOPER =
            "INSERT INTO developers(name) VALUES(:name)";

    public InsertDeveloper(DataSource ds) {
        super(ds, SQL_INSERT_DEVELOPER);
        super.declareParameter(new SqlParameter("name", Types.VARCHAR));
        super.setGeneratedKeysColumnNames("id");
        super.setReturnGeneratedKeys(TRUE);

    }
}
