package net.f.db.dao;

import net.f.db.entity.Developer;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Artyom Fyodorov
 */
public class DeveloperRowCallbackHandler implements RowCallbackHandler {
    Map<Long, Developer> results = new HashMap<>();

    @Override
    public void processRow(ResultSet rs) throws SQLException {
        long id = rs.getLong("id");
        Developer developer = new Developer();
        results.putIfAbsent(id, developer);
        developer = results.get(id);
        if (developer.getId() == null) {
            developer.setId(id);
            developer.setName(rs.getString("name"));
        }
        developer.addLanguage(rs.getString("value"));
    }
}
