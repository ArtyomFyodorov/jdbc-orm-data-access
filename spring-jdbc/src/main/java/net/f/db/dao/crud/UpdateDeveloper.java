package net.f.db.dao.crud;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

/**
 * @author Artyom Fyodorov
 */
public class UpdateDeveloper extends SqlUpdate {
    private static final String SQL_UPDATE_DEVELOPER =
            "UPDATE developers SET name = :name WHERE id = :developerId";

    public UpdateDeveloper(DataSource dataSource) {
        super(dataSource, SQL_UPDATE_DEVELOPER);
        super.declareParameter(new SqlParameter("developerId", Types.BIGINT));
        super.declareParameter(new SqlParameter("name", Types.VARCHAR));
    }
}
