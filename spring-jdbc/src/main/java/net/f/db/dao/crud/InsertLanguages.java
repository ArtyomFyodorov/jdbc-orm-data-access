package net.f.db.dao.crud;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.BatchSqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

/**
 * @author Artyom Fyodorov
 */
public class InsertLanguages extends BatchSqlUpdate {
    private static final String SQL_INSERT_DEVELOPER =
            "INSERT INTO languages VALUES(:developerId, :value)";
    private static final int BATCH_SIZE = 10;

    public InsertLanguages(DataSource ds) {
        super(ds, SQL_INSERT_DEVELOPER);
        super.declareParameter(new SqlParameter("developerId", Types.BIGINT));
        super.declareParameter(new SqlParameter("value", Types.VARCHAR));
        super.setBatchSize(BATCH_SIZE);
    }
}
