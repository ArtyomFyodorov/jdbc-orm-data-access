package net.f.db.dao.crud;

import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.SqlUpdate;

import javax.sql.DataSource;
import java.sql.Types;

/**
 * @author Artyom Fyodorov
 */
public class DeleteDeveloper extends SqlUpdate {
    private static final String SQL_DELETE_SINGERS =
            "DELETE FROM developers WHERE id = :developerId";

    public DeleteDeveloper(DataSource dataSource) {
        super(dataSource, SQL_DELETE_SINGERS);
        super.declareParameter(new SqlParameter("developerId", Types.BIGINT));
    }
}
