package net.f.db.dao;

import net.f.db.dao.crud.*;
import net.f.db.entity.Developer;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.*;
import java.util.logging.Logger;

import static java.util.Collections.singletonMap;

/**
 * @author Artyom Fyodorov
 */
@Repository
public class SpringJdbcDeveloperDao implements DeveloperDao {
    private static final Logger logger = Logger.getLogger(SpringJdbcDeveloperDao.class.getName());

    private final DataSource dataSource;
    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final InsertDeveloper insertDeveloper;
    private final UpdateDeveloper updateDeveloper;
    private final DeleteDeveloper deleteDeveloper;
    private final InsertLanguages insertLanguages;
    private final DeleteLanguages deleteLanguages;

    public SpringJdbcDeveloperDao(DataSource dataSource, NamedParameterJdbcTemplate jdbcTemplate) {
        this.dataSource = dataSource;
        this.jdbcTemplate = jdbcTemplate;
        this.insertDeveloper = new InsertDeveloper(this.dataSource);
        this.updateDeveloper = new UpdateDeveloper(this.dataSource);
        this.deleteDeveloper = new DeleteDeveloper(this.dataSource);
        this.insertLanguages = new InsertLanguages(this.dataSource);
        this.deleteLanguages = new DeleteLanguages(this.dataSource);
    }

    @Override
    public List<Developer> findAll() {
        final String sql = "SELECT d.id, d.name, l.value FROM developers AS d LEFT OUTER JOIN languages AS l ON d.id = l.developer_id";

        DeveloperRowCallbackHandler developerRowCallbackHandler = new DeveloperRowCallbackHandler();
        this.jdbcTemplate.query(sql, developerRowCallbackHandler);

        return new ArrayList<>(developerRowCallbackHandler.results.values());
    }

    @Override
    public List<Developer> findByName(String name) {
        final String sql = "SELECT d.id, d.name, l.value FROM developers AS d LEFT OUTER JOIN languages AS l ON d.id = l.developer_id WHERE d.name = :name";

        DeveloperRowCallbackHandler developerRowCallbackHandler = new DeveloperRowCallbackHandler();
        this.jdbcTemplate.query(sql, singletonMap("name", name), developerRowCallbackHandler);

        return new ArrayList<>(developerRowCallbackHandler.results.values());
    }

    @Override
    public String findNameById(Long id) {
        final String sql = "SELECT name FROM developers WHERE id = :developerId";

        return this.jdbcTemplate.queryForObject(sql, singletonMap("developerId", id), String.class);
    }

    @Override
    public Developer findById(Long id) {
        final String sql = "SELECT d.id, d.name, l.value FROM developers AS d LEFT OUTER JOIN languages AS l ON d.id = l.developer_id WHERE d.id = :developerId";

        DeveloperRowCallbackHandler developerRowCallbackHandler = new DeveloperRowCallbackHandler();
        this.jdbcTemplate.query(sql, singletonMap("developerId", id), developerRowCallbackHandler);
        Collection<Developer> values = developerRowCallbackHandler.results.values();

        return DataAccessUtils.singleResult(values);
    }

    @Override
    public Developer insert(Developer developer) {
        Map<String, String> namedParameters = singletonMap("name", developer.getName());
        logger.info("developer arrived: " + developer);

        /*
        SimpleJdbcInsert insertDeveloper = new SimpleJdbcInsert(this.dataSource)
                .withTableName("developers").usingGeneratedKeyColumns("id");
        Number newId = insertDeveloper.executeAndReturnKey(namedParameters);
        developer.setId(newId.longValue());
        */

        // more object-oriented manner using SqlUpdate
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
        this.insertDeveloper.updateByNamedParam(namedParameters, keyHolder);
        developer.setId(keyHolder.getKey().longValue());

        insertLanguages(developer.getId(), developer.getLanguages());
        logger.info("developer stored with id: " + developer.getId());

        return developer;
    }

    @Override
    public Developer update(Developer developer) {
        logger.info("developer arrived: " + developer);
        Map<String, Object> namedParameters = new HashMap<>();
        namedParameters.put("name", developer.getName());
        namedParameters.put("developerId", developer.getId());

        /*
        final String SQL_UPDATE_DEVELOPER = "UPDATE developers SET name = :name WHERE id = :developerId";
        this.jdbcTemplate.update(SQL_UPDATE_DEVELOPER, namedParameters);
        */

        // more object-oriented manner using SqlUpdate
        this.updateDeveloper.updateByNamedParam(namedParameters);

        updateLanguages(developer.getId(), developer.getLanguages());
        logger.info("developer updated with id: " + developer.getId());

        return developer;
    }

    private void insertLanguages(Long developerId, Set<String> languages) {
        /*
        Map<String, ?>[] batch = languages.stream()
                .map(language -> {
                    Map<String, Object> namedParameters = new HashMap<>();
                    namedParameters.put("developerId", developerId);
                    namedParameters.put("value", language);
                    return namedParameters;
                }).toArray((IntFunction<Map<String, ?>[]>) Map[]::new);

        final String SQL_INSERT_DEVELOPER = "INSERT INTO languages VALUES(:developerId, :value)";
        this.jdbcTemplate.batchUpdate(SQL_INSERT_DEVELOPER, batch);
        */

        // more object-oriented manner using BatchSqlUpdate
        languages.forEach(language -> {
            Map<String, Object> namedParameters = new HashMap<>();
            namedParameters.put("developerId", developerId);
            namedParameters.put("value", language);
            this.insertLanguages.updateByNamedParam(namedParameters);
        });
        this.insertLanguages.flush();
    }

    private void updateLanguages(Long developerId, Set<String> languages) {
        Map<String, Long> namedParameters = singletonMap("developerId", developerId);

        /*
        final String SQL_DELETE_LANGUAGES = "DELETE FROM languages WHERE developer_id = :developerId";
        this.jdbcTemplate.update(SQL_DELETE_LANGUAGES, namedParameters);
        */

        // more object-oriented manner using SqlUpdate
        this.deleteLanguages.updateByNamedParam(namedParameters);
        insertLanguages(developerId, languages);
    }

    @Override
    public boolean delete(Long id) {
        Map<String, Long> namedParameters = singletonMap("developerId", id);
        if (this.deleteDeveloper.updateByNamedParam(namedParameters) > 0) {
            this.deleteLanguages.updateByNamedParam(namedParameters);
            logger.info("Deleting developer with id: " + id);
            return true;
        }

        return false;
    }
}
