package net.f.db.service;

import net.f.db.dao.DeveloperDao;
import net.f.db.entity.Developer;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author Artyom Fyodorov
 */
@Service
@Transactional(readOnly = true)
public class SpringJdbcDeveloperService implements DeveloperService {
    private final DeveloperDao developerDao;

    public SpringJdbcDeveloperService(DeveloperDao developerDao) {
        this.developerDao = developerDao;
    }

    @Override
    public List<Developer> findAll() {
        return developerDao.findAll();
    }

    @Override
    public List<Developer> findByName(String name) {
        Assert.notNull(name, "name must not be null");

        return developerDao.findByName(name);
    }

    @Override
    public String findNameById(Long id) {
        Assert.notNull(id, "id must not be null");

        return developerDao.findNameById(id);
    }

    @Override
    public Developer findById(Long id) {
        Assert.notNull(id, "id must not be null");

        return developerDao.findById(id);
    }

    @Transactional
    @Override
    public Developer saveOrUpdate(Developer developer) {
        Assert.notNull(developer, "developer must not be null");

        return developer.getId() == null
                ? developerDao.insert(developer)
                : developerDao.update(developer);
    }

    @Transactional
    @Override
    public void remove(Long id) {
        Assert.notNull(id, "id must not be null");

        developerDao.delete(id);
    }

    @Transactional
    @Override
    public void createTable() {
        developerDao.createTable();
    }
}
